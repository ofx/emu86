#pragma once

namespace Emu86 {
namespace Data {

template<typename T>
class Model
{
public:
  typedef void (*DataChangeHandler)(std::string key, T value);

private:
  std::map<std::string, T> mData;

  DataChangeHandler mEvent = nullptr;

public:
  Model() {}

  T Data(const std::string& key) { return mData[key]; }

  std::map<std::string, T> Data() { return mData; }

  void SetData(const std::string& key, T value)
  {
    mData[key] = value;
    if (mEvent != nullptr) {
      mEvent(key, value);
    }
  }

  void SetData(std::map<std::string, T> data)
  {
    for (auto kv : data) {
      SetData(kv.first, kv.second);
    }
  }

  void RegisterDataChangeHandler(DataChangeHandler handler)
  {
    mEvent = handler;
  }
};

} // namespace Data
} // namespace Emu86