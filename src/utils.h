#pragma once

#define between_opcode(a, b) opcode >= a && opcode <= b

namespace Emu86 {
namespace Utils {

template <typename T>
constexpr T
Pack(bool b)
{
  return b;
}

template <typename T, typename... Types>
constexpr T
Pack(bool b, Types... args)
{
  return (b << sizeof...(Types)) | Pack<T>(args...);
}

template <typename T, typename... Types>
void
Unpack(T packed, bool& b)
{
  b = packed & 1;
}

template <typename T, typename... Types>
void
Unpack(T packed, bool& b, Types&... args)
{
  b = packed & (1 << sizeof...(Types));
  Unpack(packed, args...);
}

} // namespace Utils
} // namespace Emu86
