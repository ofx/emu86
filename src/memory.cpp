#include "prefix.h"

// Local
#include "memory.h"
#include "exception.h"

using namespace Emu86::Memory;

Memory::Memory(const unsigned long size)
  : mSize(size)
{
  for (int i = 0 ; i < size ; ++i)
  {
    mRaw.push_back(0);
  }
}

unsigned char*
Memory::GetData()
{
  return mRaw.data();
}

unsigned long
Memory::GetSize()
{
  return mSize;
}

unsigned char Memory::operator[](const int index) const
{
  return mRaw[index];
}

void
Memory::Insert(const Memory& memory, int offset)
{
  mRaw.insert(mRaw.begin() + offset, memory.mRaw.begin(), memory.mRaw.end());
}

void
Memory::LoadFromFile(const std::string& file)
{
  std::ifstream f(file, std::ios::binary);
  if (!f.good()) {
    EMU86_THROW_LINE("File '" + file + "' is unreadable");
  }

  f.unsetf(std::ios::skipws);

  f.seekg(0, std::ios::end);
  mSize = f.tellg();
  f.seekg(0, std::ios::beg);

  if (mRaw.capacity() < mSize) {
    mRaw.reserve(mSize);
  }

  mRaw.insert(mRaw.begin(), std::istream_iterator<unsigned char>(f),
              std::istream_iterator<unsigned char>());
}

void
Memory::Print()
{
  std::cout << "Memory dump (" << this->GetSize() << " bytes):" << std::endl;
  const int lw = 4 * 5;
  int i = 0;
  for (const unsigned char byte : mRaw) {
    std::cout << std::hex << std::setfill('0') << std::setw(2)
              << std::nouppercase << (int)byte << ' ';
    if (++i % lw == 0) {
      std::cout << std::endl;
    }
  }
  if (i - 1 % lw != 0) {
    std::cout << std::endl;
  }
}

Rom::Rom(const std::string& path)
{
  this->LoadFromFile(path);
}

Ram::Ram(const unsigned long size)
  : Memory(size)
{
}
