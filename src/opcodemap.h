#pragma once

// Stdlib
#include <type_traits>
#include <stdexcept>

namespace Emu86 {

class OpcodeMap
{
private:
    char* const mBegin;

    unsigned int mSize;
public:
    template<unsigned int N>
    constexpr OpcodeMap(const char(&arr)[N]) : mBegin(arr), mSize(N) 
    {
        static_assert(N >= 1, "not a string literal");
        this->Parse(0, 0);
    }

    constexpr void Parse(int i, int n, int j)
    {
        static char* buf = "";

        switch ((*this)[i])
        {
            case ' ':
            case '\t':

                ++n;
                break;
            case '\n':
                n = 0;
                break;
            default:

                break;
        }

        if (i < mSize)
            this->Parse(i + 1, n, j);
    }
    
    constexpr unsigned InRange(unsigned i, unsigned len)
    {
        if (i >= len)
            throw std::out_of_range("");
        else
            return i;
    }
 
    constexpr char operator[](unsigned i) 
    { 
        return this->InRange(i, mSize), mBegin[i]; 
    }

    constexpr operator const char *() 
    { 
        return mBegin; 
    }
 
    constexpr unsigned Size() 
    { 
        return mSize; 
    }
};

} // namespace Emu86