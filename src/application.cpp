#include "prefix.h"

// Local
#include "application.h"
#include "exception.h"

#include <cstdio>

using namespace Emu86;

Application::Application(Mode mode, Config config)
  : kConfig(config)
  , kMode(mode)
  , mIsRunning(true)
{
  try {
    mEmulator = std::make_unique<Emulator>(kMode, kConfig);
  } catch (const Exception& exception) {
    EMU86_THROW_LINE("Emulator error: " + std::string(exception.What()));
  }
}

Application::~Application()
{
}

void
Application::Run()
{
  mEmulator->Boot();

  while (mIsRunning)
  {
    mEmulator->Step();
  }
}