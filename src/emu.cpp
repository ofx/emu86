#include "prefix.h"

// Local
#include "emu.h"
#include "8086.h"
#include "controlunit.h"
#include "exception.h"
#include "model.h"

using namespace Emu86;
using namespace Emu86::Data;

Emulator::Emulator(Mode mode, Config config)
  : mMode(mode)
  , mConfig(config)
  , mRegistersModel(std::make_shared<Model<std::vector<unsigned char>>>())
{
  auto sp = std::make_shared<ControlUnit>(config);
  mControlUnit = sp;

  switch (mode) {
    case M8086:
      mCpu = std::make_unique<Cpu::Cpu8086>(sp);
      break;
    default:
      EMU86_THROW_LINE("Invalid CPU emulation mode");
      return;
  }
}

void
Emulator::Boot()
{
  std::cout << "Booting..." << std::endl;

  // Shadow ROM
  // Might want to allow for disabling this as some time.
  if (auto p = mControlUnit.lock()) {
    p->ShadowRom();
  } else {
    EMU86_THROW_LINE("Failed to acquire control unit for ROM shadowing");
  }

  // Update registers viewmodel
  mRegistersModel->SetData(mCpu->RegisterValues());
}

void
Emulator::Step()
{
  mCpu->Cycle();

  // Update registers viewmodel
  mRegistersModel->SetData(mCpu->RegisterValues());
}

void
Emulator::Run()
{}
