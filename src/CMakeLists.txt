# Emu86
set(EMU86_SRCS
  8086.h
  8086.cpp
  opcodemap.h
  alu.h
  application.h
  controlunit.h
  cpu.h
  defaults.h
  emu.h
  exception.h
  memory.h
  utils.h
  model.h
  prefix.h
  emu.cpp
  utils.cpp
  memory.cpp
  main.cpp
  alu.cpp
  controlunit.cpp
  application.cpp
  config.h
)

configure_file(config.h.in "${CMAKE_CURRENT_BINARY_DIR}/config.h" @ONLY)

add_executable(emu86 ${EMU86_SRCS})

target_include_directories(
  emu86
  PUBLIC "${CMAKE_CURRENT_BINARY_DIR}"
  PUBLIC ../external/inih
  PUBLIC ../external/nuklear
)

# getopt
if(WIN32)
  target_include_directories(
    emu86
    PUBLIC ../external/getopt
  )
endif()

# Set working directory to executable directory
set_property(TARGET emu86 PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CMAKE_CFG_INTDIR})

if(MSVC AND NOT (MSVC_VERSION LESS 1900))
  # prevent error LNK2019: unresolved external symbol _sprintf referenced in function __bdf_parse_properties
  # see http://stackoverflow.com/a/32418900/469659
  target_link_libraries(emu86 "legacy_stdio_definitions.lib")
endif()

add_custom_command(TARGET emu86 PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${CMAKE_SOURCE_DIR}/assets $<TARGET_FILE_DIR:emu86>
)

# ALU
set(ALU_SRCS
  alu.h
  utils.h
  alu.cpp
  alumain.cpp
  utils.cpp
)

add_executable(alu ${ALU_SRCS})

target_include_directories(
  alu
  PUBLIC "${CMAKE_CURRENT_BINARY_DIR}"
)

# Tests
add_test(ALU_ADD_POS alu 16 123 + 456)
set_tests_properties(ALU_ADD_POS
  PROPERTIES PASS_REGULAR_EXPRESSION "123 \\+ 456 = 579")

add_test(ALU_ADD_NEG alu 16 123 + -456)
set_tests_properties(ALU_ADD_NEG
  PROPERTIES PASS_REGULAR_EXPRESSION "123 \\+ -456 = -333")

add_test(ALU_SUB_POS alu 16 300 - 200)
set_tests_properties(ALU_SUB_POS
  PROPERTIES PASS_REGULAR_EXPRESSION "300 \\- 200 = 100")

add_test(ALU_SUB_NEG alu 16 300 - 400)
set_tests_properties(ALU_SUB_NEG
  PROPERTIES PASS_REGULAR_EXPRESSION "300 \\- 400 = -100")

add_test(ALU_AND_POS alu 16 1432 "&" 542)
set_tests_properties(ALU_AND_POS
  PROPERTIES PASS_REGULAR_EXPRESSION "1432 \\& 542 = 24")

add_test(ALU_AND_NEG alu 16 1432 "&" -542)
set_tests_properties(ALU_AND_NEG
  PROPERTIES PASS_REGULAR_EXPRESSION "1432 \\& -542 = 1408")

add_test(ALU_OR_POS alu 16 1432 | 542)
set_tests_properties(ALU_OR_POS
  PROPERTIES PASS_REGULAR_EXPRESSION "1432 \\| 542 = 1950")

add_test(ALU_OR_NEG alu 16 1432 | -542)
set_tests_properties(ALU_OR_NEG
  PROPERTIES PASS_REGULAR_EXPRESSION "1432 \\| -542 = -518")
