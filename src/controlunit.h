#pragma once

// Local
#include "emu.h"
#include "memory.h"

#define BIOS_OFFSET 0xf0000

namespace Emu86 {

class ControlUnit
{
private:
  const Config mConfig;

  std::unique_ptr<Memory::Rom> mRom;
  std::unique_ptr<Memory::Ram> mRam;

public:
  ControlUnit(Config config);

  void ShadowRom();

  unsigned char RamReadByte(const int index,
                            const unsigned char mask = 0xff) const;
  unsigned short RamReadByteSignExtendedShort(
    const int index, const unsigned char mask = 0xff) const;
  unsigned short RamReadShort(const int index) const;
};

} // namespace Emu86
