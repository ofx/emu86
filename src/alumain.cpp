// Local
#include "alu.h"

// Stdlib
#include <iostream>
#include <limits>
#include <string>

using namespace Emu86::Alu;

static inline char
OpToChar(const char op)
{
  switch (op) {
    case 0:
      return '+';
    case 1:
      return '-';
    case 2:
      return '&';
    case 3:
      return '|';
  }
  return '?';
}

static inline void
OperateAlu16(const short a, const char op, const short b)
{
  bool z, c, o;

  Alu16 alu;
  std::cout << a << " " << OpToChar(op) << " " << b << " = "
            << alu.Operate(op, a, b, z, c, o) << std::endl;
}

int
main(int argc, char* argv[])
{
  if (argc != 5) {
    std::cout << "usage: " << argv[0]
              << ": num-bits operand-a operator operand-b" << std::endl;
    return 1;
  }

  const int a = std::stoi(argv[2]);
  const int b = std::stoi(argv[4]);

  char op = argv[3][0];
  switch (op) {
    case '+':
      op = 0;
      break;
    case '-':
      op = 1;
      break;
    case '&':
      op = 2;
      break;
    case '|':
      op = 3;
      break;
    default:
      std::cout << "unknown operator '" << op << "', should be one of (&,|,+,-)"
                << std::endl;
      return 5;
  }

  const int bits = std::stoi(argv[1]);
  switch (bits) {
    case 16:
      if (a > std::numeric_limits<short>::max() ||
          a < std::numeric_limits<short>::min()) {
        std::cout << "operand-a exceeds boundaries" << std::endl;
        return 3;
      }
      if (b > std::numeric_limits<short>::max() ||
          b < std::numeric_limits<short>::min()) {
        std::cout << "operand-b exceeds boundaries" << std::endl;
        return 4;
      }
      OperateAlu16(a, op, b);
      break;
    default:
      std::cout << "no support for " << bits << " bits ALU" << std::endl;
      return 2;
  }
}
