#include "prefix.h"

// Local
#include "controlunit.h"

using namespace Emu86;

ControlUnit::ControlUnit(Config config)
  : mConfig(config)
  , mRam(std::make_unique<Memory::Ram>(mConfig.RamSize))
  , mRom(std::make_unique<Memory::Rom>(mConfig.RomPath))
{
  if (!mConfig.RamPath.empty()) {
    mRam->LoadFromFile(mConfig.RamPath);
  }
}

void
ControlUnit::ShadowRom()
{
  mRam->Insert(*mRom, BIOS_OFFSET);
}

unsigned char
ControlUnit::RamReadByte(const int index, const unsigned char mask) const
{
  return (*mRam)[index] & mask;
}

unsigned short
ControlUnit::RamReadByteSignExtendedShort(const int index,
                                          const unsigned char mask) const
{
  const auto b = this->RamReadByte(index, mask);
  unsigned short r = b & 0x40 ? 0xffff : 0;
  r |= b;
  return r;
}

unsigned short
ControlUnit::RamReadShort(const int index) const
{
  return (unsigned short)(((unsigned short)(*mRam)[index + 1]) << 8) |
         (*mRam)[index];
}
