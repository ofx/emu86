#pragma once

// Local
#include "cpu.h"
#include "opcodemap.h"

// 16-bit
#define WORD unsigned short
#define BYTE unsigned char

#define RESET_VECTOR_CS 0xffff
#define RESET_VECTOR_IP 0x0000

namespace Emu86 {

class ControlUnit;

namespace Cpu {

class Registers8086
{
private:
  std::array<unsigned char, 28> mRaw;

public:
  union Word
  {
    std::vector<unsigned char> Bytes;
    short WordValue;
  };
  union Byte
  {
    std::vector<unsigned char> Bytes;
    unsigned char Byte;
  };

  enum WordNames : unsigned char
  {
    AX = 0,
    BX = 2,
    CX = 4,
    DX = 6,
    SI = 8,
    DI = 10,
    BP = 12,
    SP = 14,
    IP = 16,
    CS = 18,
    DS = 20,
    ES = 22,
    SS = 24,
    FL = 26
  };
  enum ByteNames : unsigned char
  {
    AH = 0,
    AL = 1,
    BH = 2,
    BL = 3,
    CH = 4,
    CL = 5,
    DH = 6,
    DL = 7
  };
  enum FlagNames : unsigned char
  {
    O = 4,
    D = 5,
    I = 6,
    T = 7,
    S = 8,
    Z = 9,
    A = 11,
    P = 13,
    C = 15
  };

  Registers8086();
  ~Registers8086();

  unsigned short& operator[](const WordNames name);
  const unsigned short& operator[](const WordNames name) const;

  unsigned char& operator[](const ByteNames name);
  const unsigned char& operator[](const ByteNames name) const;

  void EncodeFlag(FlagNames name, bool value);
  bool DecodeFlag(FlagNames name);

  std::map<std::string, std::vector<unsigned char>> ToMap();
};

class Cpu8086 : public Cpu
{
private:
  std::unique_ptr<Alu::Alu16> mAlu;

  std::unique_ptr<Registers8086> mRegisters;

  struct ModRegRM
  {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    unsigned char RM : 3;
    unsigned char Reg : 3;
    unsigned char Mod : 2;
#elif __BYTE_ORDER == __BIG_ENDIAN
    unsigned char Mod : 2;
    unsigned char Reg : 3;
    unsigned char RM : 3;
#else
#error "Please fix <bits/endian.h>"
#endif
  };

  Registers8086::WordNames DecodeRegWord(unsigned char reg) const;
  Registers8086::ByteNames DecodeRegByte(unsigned char reg) const;

public:
  Cpu8086(std::shared_ptr<ControlUnit> controlUnit);
  ~Cpu8086();

  void Cycle() override;

  std::map<std::string, std::vector<unsigned char>> RegisterValues() override
  {
    return mRegisters->ToMap();
  }
};

} // namespace Cpu
} // namespace Emu86
