#pragma once

namespace Emu86 {

class Exception : public std::runtime_error
{
private:
  std::string mMsg;

public:
  Exception(const std::string& arg, const char* file, int line)
    : std::runtime_error(arg)
  {
    std::ostringstream o;
    o << file << ":" << line << ": " << arg;
    mMsg = o.str();
  }

  ~Exception() throw() {}

  const char* What() const throw() { return mMsg.c_str(); }
};

} // namespace Emu86

#define EMU86_THROW_LINE(arg) throw Emu86::Exception(arg, __FILE__, __LINE__);
