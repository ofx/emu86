#pragma once

// Stdlib
#include <string>
#include <vector>

namespace Emu86 {
namespace Memory {

class Memory
{
private:
  std::vector<unsigned char> mRaw;

  unsigned long mSize;

public:
  Memory(const unsigned long size = 0);

  unsigned char* GetData();

  unsigned long GetSize();

  unsigned char operator[](const int index) const;

  void Insert(const Memory& memory, int offset = 0);

  void LoadFromFile(const std::string& file);

  void Print();
};

class Rom : public Memory
{
private:
public:
  Rom(const std::string& path);
};

class Ram : public Memory
{
private:
public:
  Ram(const unsigned long size);
};

} // namespace Memory
} // namespace Emu86
