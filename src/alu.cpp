// Local
#include "alu.h"
#include "utils.h"

using namespace Emu86::Alu;
using namespace Emu86::Utils;

inline unsigned char
Alu16::Adder(const unsigned short a, const unsigned short b, const bool cin,
             bool& cout)
{
  cout = (a & b) | (cin & (a ^ b));
  return cin ^ (a ^ b);
}

short
Alu16::Operate(const unsigned char control, const unsigned short a,
               const unsigned short b, bool& z, bool& c, bool& o) const
{
  short r = 0, i;

  // Multiplex
  switch (control) {
    case ADD:
    case SUB: {
      const unsigned char a1 = BIT(a & (1 << 0x0));
      const unsigned char a2 = BIT(a & (1 << 0x1));
      const unsigned char a3 = BIT(a & (1 << 0x2));
      const unsigned char a4 = BIT(a & (1 << 0x3));
      const unsigned char a5 = BIT(a & (1 << 0x4));
      const unsigned char a6 = BIT(a & (1 << 0x5));
      const unsigned char a7 = BIT(a & (1 << 0x6));
      const unsigned char a8 = BIT(a & (1 << 0x7));
      const unsigned char a9 = BIT(a & (1 << 0x8));
      const unsigned char a10 = BIT(a & (1 << 0x9));
      const unsigned char a11 = BIT(a & (1 << 0xA));
      const unsigned char a12 = BIT(a & (1 << 0xB));
      const unsigned char a13 = BIT(a & (1 << 0xC));
      const unsigned char a14 = BIT(a & (1 << 0xD));
      const unsigned char a15 = BIT(a & (1 << 0xE));
      const unsigned char a16 = BIT(a & (1 << 0xF));

      const unsigned char b1 = (BIT(b & (1 << 0x0))) ^ control;
      const unsigned char b2 = (BIT(b & (1 << 0x1))) ^ control;
      const unsigned char b3 = (BIT(b & (1 << 0x2))) ^ control;
      const unsigned char b4 = (BIT(b & (1 << 0x3))) ^ control;
      const unsigned char b5 = (BIT(b & (1 << 0x4))) ^ control;
      const unsigned char b6 = (BIT(b & (1 << 0x5))) ^ control;
      const unsigned char b7 = (BIT(b & (1 << 0x6))) ^ control;
      const unsigned char b8 = (BIT(b & (1 << 0x7))) ^ control;
      const unsigned char b9 = (BIT(b & (1 << 0x8))) ^ control;
      const unsigned char b10 = (BIT(b & (1 << 0x9))) ^ control;
      const unsigned char b11 = (BIT(b & (1 << 0xA))) ^ control;
      const unsigned char b12 = (BIT(b & (1 << 0xB))) ^ control;
      const unsigned char b13 = (BIT(b & (1 << 0xC))) ^ control;
      const unsigned char b14 = (BIT(b & (1 << 0xD))) ^ control;
      const unsigned char b15 = (BIT(b & (1 << 0xE))) ^ control;
      const unsigned char b16 = (BIT(b & (1 << 0xF))) ^ control;

      const unsigned char r1 = Alu16::Adder(a1, b1, control, c);
      const unsigned char r2 = Alu16::Adder(a2, b2, c, c);
      const unsigned char r3 = Alu16::Adder(a3, b3, c, c);
      const unsigned char r4 = Alu16::Adder(a4, b4, c, c);
      const unsigned char r5 = Alu16::Adder(a5, b5, c, c);
      const unsigned char r6 = Alu16::Adder(a6, b6, c, c);
      const unsigned char r7 = Alu16::Adder(a7, b7, c, c);
      const unsigned char r8 = Alu16::Adder(a8, b8, c, c);
      const unsigned char r9 = Alu16::Adder(a9, b9, c, c);
      const unsigned char r10 = Alu16::Adder(a10, b10, c, c);
      const unsigned char r11 = Alu16::Adder(a11, b11, c, c);
      const unsigned char r12 = Alu16::Adder(a12, b12, c, c);
      const unsigned char r13 = Alu16::Adder(a13, b13, c, c);
      const unsigned char r14 = Alu16::Adder(a14, b14, c, c);
      const unsigned char r15 = Alu16::Adder(a15, b15, c, c);
      const unsigned char r16 = Alu16::Adder(a16, b16, c, c);

      r = r | (r1 << 0x0) | (r2 << 0x1) | (r3 << 0x2) | (r4 << 0x3) |
          (r5 << 0x4) | (r6 << 0x5) | (r7 << 0x6) | (r8 << 0x7) | (r9 << 0x8) |
          (r10 << 0x9) | (r11 << 0xA) | (r12 << 0xB) | (r13 << 0xC) |
          (r14 << 0xD) | (r15 << 0xE) | (r16 << 0xF);

      o = (!r16 & b16 & a16) | (r16 & !b16 & !a16);

      break;
    }
    case AND:
      r = a & b;
      break;
    case OR:
      r = a | b;
      break;
  }

  z = !BIT(r | 0);

  return r;
}
