#pragma once

// Local
#include "cpu.h"

namespace Emu86 {

namespace Data {
template <typename T>
class Model;
}

enum Mode : unsigned char
{
  M8086,
  INVALID
};
const std::vector<std::string> ModeNames = { "8086" };

struct Config
{
  std::string RomPath;
  std::string RamPath;
  std::string FloppyPath;

  unsigned long RamSize;

  bool IsValid(std::string& err)
  {
    if (RomPath.empty()) {
      err = "ROM path cannot be empty";
      return false;
    } else if (RamSize == 0) {
      err = "RAM size cannot be 0";
      return false;
    }
    return true;
  }
};

class ControlUnit;

class Emulator
{
private:
  const Mode mMode;

  const Config mConfig;

  std::unique_ptr<Cpu::Cpu> mCpu;

  std::weak_ptr<ControlUnit> mControlUnit;

  std::shared_ptr<Data::Model<std::vector<unsigned char>>> mRegistersModel;

public:
  Emulator(Mode mode, Config config);

  std::weak_ptr<Data::Model<std::vector<unsigned char>>> RegistersModel()
  {
    return mRegistersModel;
  }

  void Boot();
  void Step();
  void Run();
};

} // namespace Emu86
