#pragma once

// Local
#include "emu.h"

namespace Emu86 {

class Application
{
private:
  const Emu86::Config kConfig;
  const Emu86::Mode kMode;

  bool mIsRunning;

  std::unique_ptr<Emulator> mEmulator;

public:
  Application(Mode mode, Config config);
  ~Application();

  void Run();
};

} // namespace Emu86