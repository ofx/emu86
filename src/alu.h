#pragma once

// 16-bits ALU

#define ADD 0
#define SUB 1
#define AND 2
#define OR 3

#define BIT(b) (!!(b))

namespace Emu86 {
namespace Alu {

class Alu
{
  virtual short Operate(const unsigned char control, const unsigned short a,
                        const unsigned short b, bool& z, bool& c,
                        bool& o) const = 0;
};

class Alu16 : public Alu
{
private:
  static inline unsigned char Adder(const unsigned short a,
                                    const unsigned short b, const bool cin,
                                    bool& cout);

public:
  Alu16() {}

  short Operate(const unsigned char control, const unsigned short a,
                const unsigned short b, bool& z, bool& c,
                bool& o) const override;
};

} // namespace Alu
} // namespace Emu86
