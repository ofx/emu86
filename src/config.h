#pragma once

#define EMU86_MAJOR_VERSION (0)
#define EMU86_MINOR_VERSION (1)
#define EMU86_PATCH_VERSION (0)
#define EMU86_VERSION_STRING "0.1.0"
#define EMU86_WINDOW_TITLE "Emu86 - " EMU86_VERSION_STRING
