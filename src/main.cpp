#include "prefix.h"

// Local
#include "application.h"
#include "config.h"
#include "defaults.h"
#include "emu.h"
#include "exception.h"

// inih
#include "INIReader.h"

// getopt
#include "getopt.h"

#ifdef WIN32
#include <vld.h>
#endif

using namespace Emu86;

static Mode gMode;

static Config gConfig;

void
help(const std::string& exe)
{
  std::cout << "usage: " << exe << " [-mlh]" << std::endl
            << "\t-m\t Emulation mode, use -l for listing of modes" << std::endl
            << "\t-l\t List emulation modes" << std::endl
            << "\t-h\t This help message" << std::endl;
}

void
list_modes()
{
  std::cout << "Emulation modes:" << std::endl;
  for (const auto& name : ModeNames) {
    std::cout << '\t' << name << std::endl;
  }
}

Mode
match_mode(std::string mode)
{
  int i = 0;
  for (const auto& name : ModeNames) {
    if (name == mode) {
      return static_cast<Mode>(i);
    }
    ++i;
  }
  return Mode::INVALID;
}

bool
read_config(std::string configPath)
{
  std::ifstream f(configPath);
  if (f.good()) {
    INIReader reader(configPath);

    gConfig.RamPath = reader.Get("ram", "path", EMU86_DEFAULT_RAM_PATH);
    gConfig.RomPath = reader.Get("rom", "path", EMU86_DEFAULT_ROM_PATH);
    gConfig.FloppyPath =
      reader.Get("floppy", "path", EMU86_DEFAULT_FLOPPY_PATH);

    std::string ramSize = reader.Get("ram", "size", EMU86_DEFAULT_RAM_SIZE);
    const char prefix[] = { 'K', 'M', 'G' };
    const unsigned int size = sizeof(prefix);
    unsigned long power = 1;
    for (int i = 1; i <= size; ++i) {
      std::string::size_type pos = ramSize.find(prefix[i - 1]);
      if (pos != std::string::npos) {
        ramSize.erase(pos);
        power = i;
      }
    }

    gConfig.RamSize = std::pow(1024, power) * std::stoul(ramSize);

    std::string err;
    if (!gConfig.IsValid(err)) {
      std::cout << "Invalid configuration: " << err << '.' << std::endl;
      return false;
    }

    std::cout << "Configuration:" << std::endl;
    std::cout << "\tRAM Path: " << gConfig.RamPath << std::endl;
    std::cout << "\tROM Path: " << gConfig.RomPath << std::endl;
    std::cout << "\tFloppy Path: " << gConfig.FloppyPath << std::endl;
    std::cout << "\tRAM Size: " << gConfig.RamSize << " bytes" << std::endl;
  } else {
    std::cout << "Could not read configuration file: " << configPath
              << std::endl;

    return false;
  }

  return true;
}

bool
parse_arguments(int argc, char* argv[])
{
  std::string exe = argv[0];
  std::string modeStr = EMU86_DEFAULT_MODE;
  std::string configPath = EMU86_DEFAULT_CONFIG_PATH;

  int c;
  while ((c = getopt(argc, argv, "m:hlc:")) != -1) {
    switch (c) {
      case 'h':
        help(exe);
        return false;
      case 'm':
        modeStr = std::string(optarg);
        break;
      case 'l':
        list_modes();
        return false;
      case 'c':
        configPath = std::string(optarg);
        break;
      case '?':
        if (optopt == 'm') {
          std::cout << "Option 'm' requires an argument." << std::endl;
          help(exe);
          return false;
        }
      default:
        std::cout << "Unknown error while parsing arguments." << std::endl;
        return false;
    }
  }

  gMode = match_mode(modeStr);
  if (gMode == Mode::INVALID) {
    std::cout << "Invalid emulator mode '" << modeStr << "'." << std::endl;
    return false;
  }

  if (!read_config(configPath)) {
    return false;
  }

  return true;
}

int
main(int argc, char* argv[])
{
  std::cout << "Emu86 - " << EMU86_VERSION_STRING << std::endl << std::endl;

  if (!parse_arguments(argc, argv)) {
    return 1;
  }

  Application application(gMode, gConfig);
  application.Run();

  return 0;
}
