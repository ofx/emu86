#include "prefix.h"

// Local
#include "8086.h"
#include "alu.h"
#include "controlunit.h"
#include "exception.h"
#include "utils.h"

using namespace Emu86::Cpu;

Registers8086::Registers8086()
{
  mRaw.fill(0);

  // Reset vector
  (*this)[WordNames::CS] = RESET_VECTOR_CS;
  (*this)[WordNames::IP] = RESET_VECTOR_IP;
}

Registers8086::~Registers8086() {}

unsigned short& Registers8086::operator[](const WordNames name)
{
  return *reinterpret_cast<unsigned short*>(&mRaw[static_cast<int>(name)]);
}

const unsigned short& Registers8086::operator[](const WordNames name) const
{
  return *reinterpret_cast<const unsigned short*>(
    &mRaw[static_cast<int>(name)]);
}

unsigned char& Registers8086::operator[](const ByteNames name)
{
  return mRaw[static_cast<int>(name)];
}

const unsigned char& Registers8086::operator[](const ByteNames name) const
{
  return mRaw[static_cast<int>(name)];
}

void
Registers8086::EncodeFlag(FlagNames name, bool value)
{
  if (value) {
    (*this)[WordNames::FL] |= 1UL << static_cast<unsigned int>(name);
  } else {
    (*this)[WordNames::FL] &= ~(1UL << static_cast<unsigned int>(name));
  }
}

bool
Registers8086::DecodeFlag(FlagNames name)
{
  return ((*this)[WordNames::FL] >> static_cast<unsigned int>(name)) & 1U;
}

std::map<std::string, std::vector<unsigned char>>
Registers8086::ToMap()
{
  std::map<std::string, WordNames> wordRegisters = {
    { "AX", AX }, { "BX", BX }, { "CX", CX }, { "DX", DX }, { "SI", SI },
    { "DI", DI }, { "BP", BP }, { "SP", SP }, { "IP", IP }, { "CS", CS },
    { "DS", DS }, { "ES", ES }, { "SS", SS }, { "FL", FL } };
  std::map<std::string, ByteNames> byteRegisters = {
    { "AH", AH }, { "AL", AL }, { "BH", BH }, { "BL", BL },
    { "CH", CH }, { "CL", CL }, { "DH", DH }, { "DL", DL } };

  auto registers = *this;
  std::map<std::string, std::vector<unsigned char>> v;
  for (auto pair : wordRegisters) {
    v[pair.first] = std::vector<unsigned char>(2);
    std::memcpy(v[pair.first].data(), &registers[pair.second], 2);
  }
  for (auto pair : byteRegisters) {
    v[pair.first] = std::vector<unsigned char>(1);
    std::memcpy(v[pair.first].data(), &registers[pair.second], 1);
  }
  return v;
}

Cpu8086::Cpu8086(std::shared_ptr<Emu86::ControlUnit> controlUnit)
  : Cpu(controlUnit)
  , mAlu(std::make_unique<Alu::Alu16>())
  , mRegisters(std::make_unique<Registers8086>())
{}

Cpu8086::~Cpu8086() {}

Registers8086::WordNames
Cpu8086::DecodeRegWord(unsigned char reg) const
{
  switch (reg & 0b111) {
    case 0b000:
      return Registers8086::WordNames::AX;
    case 0b001:
      return Registers8086::WordNames::CX;
    case 0b010:
      return Registers8086::WordNames::DX;
    case 0b011:
      return Registers8086::WordNames::BX;
    case 0b100:
      return Registers8086::WordNames::SP;
    case 0b101:
      return Registers8086::WordNames::BP;
    case 0b110:
      return Registers8086::WordNames::SI;
    case 0b111:
      return Registers8086::WordNames::DI;
  }
}

Registers8086::ByteNames
Cpu8086::DecodeRegByte(unsigned char reg) const
{
  switch (reg & 0b111) {
    case 0b000:
      return Registers8086::ByteNames::AL;
    case 0b001:
      return Registers8086::ByteNames::CL;
    case 0b010:
      return Registers8086::ByteNames::DL;
    case 0b011:
      return Registers8086::ByteNames::BL;
    case 0b100:
      return Registers8086::ByteNames::AH;
    case 0b101:
      return Registers8086::ByteNames::CH;
    case 0b110:
      return Registers8086::ByteNames::DH;
    case 0b111:
      return Registers8086::ByteNames::BH;
  }
}

void
Cpu8086::Cycle()
{
  auto ip = ((*mRegisters)[Registers8086::WordNames::CS] << 4) + (*mRegisters)[Registers8086::WordNames::IP];
  const auto opcode = mControlUnit->RamReadByte(ip);

  std::cout << "op: " << std::hex << (int)opcode << std::endl;

  if (opcode == 0x81) { // GRP1 Ev Iv W=1 D=0
    const auto b = mControlUnit->RamReadByte(++ip);
    const auto modregrm = *reinterpret_cast<const ModRegRM*>(&b);
    const auto im = mControlUnit->RamReadShort(++ip);

    ip += 2;

    switch (modregrm.Reg) {
      case 0: // ADD

        break;
      case 1: // OR

        break;
      case 2: // ADC

        break;
      case 3: // SBB

        break;
      case 4: // AND

        break;
      case 5: // SUB

        break;
      case 6: // XOR

        break;
      case 7: // CMP
        bool z, c, o;
        mAlu->Operate(SUB, (*mRegisters)[this->DecodeRegWord(modregrm.RM)], im,
                      z, c, o);
        mRegisters->EncodeFlag(Registers8086::FlagNames::Z, z);
        mRegisters->EncodeFlag(Registers8086::FlagNames::C, c);
        mRegisters->EncodeFlag(Registers8086::FlagNames::O, o);
        break;
      default:
        // TODO: Signal error
        std::stringstream ss;
        ss << "Invalid extension code (" << std::hex << modregrm.Reg << ")";
        EMU86_THROW_LINE(ss.str());
    }
  } else if (opcode == 0x74) { // JZ/JE Jbs
    const char offset = mControlUnit->RamReadByte(++ip);

    ip += 1;

    if (mRegisters->DecodeFlag(Registers8086::FlagNames::Z)) {
      ip += offset;
    }
  } else if (opcode == 0x75) { // JNZ/JNE
    const char offset = mControlUnit->RamReadByte(++ip);

    ip += 1;

    if (!mRegisters->DecodeFlag(Registers8086::FlagNames::Z)) {
      ip += offset;
    }
  } else if (opcode == 0xf4) { // HLT
    // TODO
  } else if (between_opcode(0xb0, 0xb7)) { // MOV Zb Ib
    const auto reg = this->DecodeRegWord(opcode & 7);
    const auto im = mControlUnit->RamReadByte(++ip);

    ip += 1;

    (*mRegisters)[reg] = im;
  } else if (between_opcode(0xb8, 0xbf)) { // MOV Zv Iv
    const auto reg = this->DecodeRegWord(opcode & 7);
    const auto im = mControlUnit->RamReadShort(++ip);

    ip += 2;

    (*mRegisters)[reg] = im;
  } else if (between_opcode(0x48, 0x4f)) { // DEC Zv
    const auto reg = this->DecodeRegWord(opcode & 7);

    ip += 1;

    --(*mRegisters)[reg];
  } else if (opcode == 0x83) { // GRP3 Ev Ibs
    const auto b = mControlUnit->RamReadByte(++ip);
    const auto modregrm = *reinterpret_cast<const ModRegRM*>(&b);
    const auto im = mControlUnit->RamReadByteSignExtendedShort(++ip);

    ip += 2;

    switch (modregrm.Reg) {
      case 0: // ADD

        break;
      case 1: // OR

        break;
      case 2: // ADC

        break;
      case 3: // SBB

        break;
      case 4: // AND

        break;
      case 5: // SUB

        break;
      case 6: // XOR

        break;
      case 7: // CMP
        bool z, c, o;
        mAlu->Operate(SUB, (*mRegisters)[this->DecodeRegWord(modregrm.RM)], im,
                      z, c, o);
        mRegisters->EncodeFlag(Registers8086::FlagNames::Z, z);
        mRegisters->EncodeFlag(Registers8086::FlagNames::C, c);
        mRegisters->EncodeFlag(Registers8086::FlagNames::O, o);
        break;
    }
  } else if (opcode == 0xcd) {
    
  } else if (opcode == 0xe9) { // JMP	rel16/32
    const short offset = mControlUnit->RamReadShort(++ip);
    std::cout << offset << std::endl;

    if (true) {
    
    }
  } else {
    // TODO: Signal error
    std::stringstream ss;
    ss << "Invalid opcode (" << std::hex << (int)opcode << ")";
    EMU86_THROW_LINE(ss.str());
  }
}
