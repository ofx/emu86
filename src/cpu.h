#pragma once

// Local
#include "alu.h"

namespace Emu86 {

class ControlUnit;

namespace Cpu {

class Cpu
{
protected:
  std::shared_ptr<ControlUnit> mControlUnit;

public:
  Cpu(std::shared_ptr<ControlUnit> controlUnit)
    : mControlUnit(controlUnit)
  {}
  virtual ~Cpu() {}

  virtual void Cycle() = 0;

  virtual std::map<std::string, std::vector<unsigned char>> RegisterValues() = 0;
};

} // namespace Cpu
} // namespace Emu86
