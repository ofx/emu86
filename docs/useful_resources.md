# Useful Resources

## General
- https://en.wikipedia.org/wiki/Zero_flag

## x86-General
- http://ref.x86asm.net/geek32.html
- https://www.swansontec.com/sintel.html
- https://www.cs.virginia.edu/~evans/cs216/guides/x86.html
- https://www.codeproject.com/articles/662301/x-instruction-encoding-revealed-bit-twiddling-fo
- https://software.intel.com/en-us/articles/intel-sdm
- https://en.wikipedia.org/wiki/Reset_vector
- https://wiki.osdev.org/Memory_Map_%28x86%29#Overview

## ASM
- https://www.codeproject.com/articles/662301/x-instruction-encoding-revealed-bit-twiddling-fo
- http://www.plantation-productions.com/Webster/www.artofasm.com/Windows/HTML/AoATOC.html
- http://www.plantation-productions.com/Webster/www.artofasm.com/Windows/HTML/ISA.html
- http://marin.jb.free.fr/jumps/

## 8086-Specific
- http://www.mlsite.net/8086/
- http://www.mlsite.net/8086/#addr_E
- http://aturing.umcs.maine.edu/~meadow/courses/cos335/8086-instformat.pdf
- http://www.retroarchive.org/dos/docs/ibm5160techref.pdf

## Mod/Reg/RM
- http://www.c-jump.com/CIS77/CPU/x86/X77_0060_mod_reg_r_m_byte.htm
- http://www.c-jump.com/CIS77/CPU/x86/lecture.html
- http://www.sparksandflames.com/files/x86InstructionChart.html

## ALU
- http://minnie.tuhs.org/CompArch/Tutes/week02.html
